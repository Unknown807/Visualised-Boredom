# Visualised-Boredom

## Description

A program that allows you to interactively use the BoredAPI (https://www.boredapi.com/) and get back responses in a graphical way.

## How To Use

<table align="center"><tr><td align="center" width="9999">

The query options will be at the top and they let you choose what type of activity you want to do, what the cost and accessibility of the activity should be and how many people you want to participate (doesn't return any activites greater than 5).

![alt text](/repo_imgs/img1.JPG)

You can also specify a price and accessibility range if you want to get results back according to said range.

![alt text](/repo_imgs/img2.JPG)

If you don't want to include certain parameters, then if you click on the parameter labels they will be omitted from the query. Additionally, by selecting the dropdown to get the price and accessibility ranges, it will omit the above specific price or accessibility.

![alt text](/repo_imgs/img3.JPG)

To use the parameters when sending the query, press the 'Find Something To Do' label beneath the options. To skip having to omit the options, to get a truly random activity, press the large text in the middle of the page that displays the activities.

![alt text](/repo_imgs/img4.JPG)
</td></tr></table>
